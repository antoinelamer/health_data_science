# Biology pivot

## Contexte

Au cours d'un séjour hospitalier, les patients ... examens de biologie.

Les résultats de biologie sont présentés comme suit, avec une ligne par résultat de biologie pour un patient.

![](https://gitlab.com/antoinelamer/health_data_science/-/raw/master/data_management/biology_pivot/images/biology_rows.png)

Pour exploiter ces données, nous avons besoin d'une ligne par patient, avec une colonne par résultat de biologie par jour.

![](https://gitlab.com/antoinelamer/health_data_science/-/raw/master/data_management/biology_pivot/images/biology_columns.png)

**Problèmes :**

-  date de J0 pour le premier patient n'est pas forcément égale à la date du J0 pour le second patient.
-  tous les patients n'ont pas forcément le même nombre de mesures, ni le même type

## Méthode

`pivot_wider()` du package [tydir](https://tidyr.tidyverse.org/)

1. Date index pour définir J0, J1, J2, J3
2. Filtre pour éliminer les enregistrements > à J20, J21 ...
3. Agrégation si plusieurs valeurs pour le même jour/examen
4. Pivot sur la date et le type d'examen pour passer les valeurs des lignes vers les colonnes

![](https://gitlab.com/antoinelamer/health_data_science/-/raw/master/data_management/biology_pivot/images/biology_pivot.png)

### 1. Date index

Date de référence, ex : date de passage au bloc opératoire.

```r
biology$index_date = as.numeric(difftime(biology$biology_date, biology$event_date, units = 'days'))
```

### 2. Filtre

```r
biology = biology %>%
  filter(index_date <= 2)
```

### 3. Agrégation

```r
biology_agg = biology %>%
  dplyr::select(-c(biology_date, biology_hour, event_date)) %>%
  dplyr::group_by(patient_id, index_date, biology_id) %>%
  dplyr::summarise(min = min(biology_result),
                   max = max(biology_result))
```

### 4. Pivot

```r
biology_agg %>%
  pivot_wider(names_from = c(biology_id, index_date),
              values_from = c(min, max))
```


## Références
https://tidyr.tidyverse.org/dev/articles/pivot.html<br />
https://www.storybench.org/pivoting-data-from-columns-to-rows-and-back-in-the-tidyverse/